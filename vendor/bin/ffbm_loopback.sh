#!/vendor/bin/sh


function handset_playback()
{
	#echo "handset_playback start.\n"
	rx_vol=$rx_handset_default
	ffbm_audio_test $file_path -D 0 -d 0 -tc 1 -t $duration_time -v $rx_vol
	#echo "handset_playback stop.\n"
}

function headset_playback()
{
	#echo "headset_playback start.\n"
	ffbm_audio_test $file_path -D 0 -d 0 -tc 2 -t $duration_time -v $rx_vol
	#echo "handset_playback stop.\n"
}

function speaker_playback()
{
	#echo "speaker_playback start.\n"
	rx_vol=$rx_spk_default
	ffbm_audio_test $file_path -D 0 -d 0 -tc 3 -t $duration_time -v $rx_vol
	#echo "speaker_playback stop.\n"
}

function handmic_capture()
{
	#echo "handmic_capture start.\n"
	ffbm_audio_test $file_path -D 0 -d 0 -tc 4 -t $duration_time -v $rx_vol
	#echo "handmic_capture stop.\n"
}

function headmic_capture()
{
	#echo "headmic_capture start.\n"
	ffbm_audio_test $file_path -D 0 -d 0 -tc 6 -t $duration_time -v $rx_vol
	#echo "headmic_capture stop.\n"
}

function handmic_spk_loopback()
{
	#echo "do handmic to spk loop."
	rx_vol=$rx_spk_default
	ffbm_audio_test $file_path -D 0 -d 1 -tc 10 -t $duration_time -v $rx_vol
	#echo "handmic_spk_loopback test end."
}

function handmic_handset_loopback()
{
	#echo "do handmic to handset loop."
	rx_vol=$rx_handset_default
	ffbm_audio_test $file_path -D 0 -d 1 -tc 12 -t $duration_time -v $rx_vol
	#echo "handmic_handset_loopback test end."
}

function handmic_headset_loopback()
{
	#echo "do handmic to headset loop."
	ffbm_audio_test $file_path -D 0 -d 1 -tc 15 -t $duration_time -v $rx_vol
	#echo "handmic_headset_loopback test end."
}

function headmic_speaker_loopback()
{
	#echo "do headmic to speaker loop."
	rx_vol=$rx_spk_default
	ffbm_audio_test $file_path -D 0 -d 1 -tc 16 -t $duration_time -v $rx_vol
	#echo "headmic_speaker_loopback test end."
}

function headsetmic_handset_loopback()
{
	#echo "do headsetmic to handset loop."
	rx_vol=$rx_handset_default
	ffbm_audio_test $file_path -D 0 -d 1 -tc 14 -t $duration_time -v $rx_vol
	#echo "headsetmic_handset_loopback test end."
}

function headmic_headset_loopback()
{
	#echo "do headmic to headset loop."
	ffbm_audio_test $file_path -D 0 -d 1 -tc 7 -t $duration_time -v $rx_vol
	#echo "headmic_headset_loopback test end."
}
#############################################################################
### ffbm test main entry
#############################################################################
file_path_default="/data/vendor/audio/amt.wav"
duration_time_default=3
rx_vol_default=84
rx_spk_default=80
rx_handset_default=87

function main()
{
	### check parameter number ###
	if [ $# -lt 5 ]
	then
		log -p e -t ffbm_loopback "please input command as: case_id [file] [time] [volume] [start/stop]"
		return
	fi

	### check file path parameter ###

	if [ ! -f "$2" ];then
		file_path=$file_path_default
		log -p e -t ffbm_loopback "file_path: $2 not exists,set $file_path_default"
	else
		file_path=$2
	fi

	duration_time=$duration_time_default
	if [ -n "$3" ]
	then
		duration_time=$3
	fi

	rx_vol=$rx_vol_default
	if [ -n "$4" ]
	then
		rx_vol=$4
	fi

	case $1 in
		handset_playback_test)
			handset_playback
		;;
		headset_playback_test)
			headset_playback
		;;
		speaker_playback_test)
			speaker_playback
		;;
		handmic_capture_test)
			handmic_capture
		;;
		headmic_capture_test)
			headmic_capture
		;;
		headmic_headset_loopback_test)
			headmic_headset_loopback
		;;
		handmic_spk_loopback_test)
		    handmic_spk_loopback
		;;
		handmic_handset_loopback_test)
			handmic_handset_loopback
		;;
		headmic_handset_loopback_test)
			headsetmic_handset_loopback
		;;
		handmic_headset_loopback_test)
			handmic_headset_loopback
		;;
		headmic_speaker_loopback_test)
			headmic_speaker_loopback
		;;
		*)
		log -p e -t ffbm_loopback "Can't find  test case!"
		return
		;;
	esac
}

log -p d -t ffbm_loopback "enter in"

loop_param=`getprop persist.vendor.audio.ffbmaudiotest`
log -p d -t ffbm_loopback "loop_param: $loop_param"

main $loop_param
log -p d -t ffbm_loopback "enter out"

exit 0
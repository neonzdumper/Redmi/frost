
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 16);

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '732' AND MNC='101';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('732','101','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('732','101','123','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('732','101','911','','');

COMMIT TRANSACTION;



# Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

on init
    wait /dev/block/platform/0.soc/${ro.boot.bootdevice}
    symlink /dev/block/platform/0.soc/${ro.boot.bootdevice} /dev/block/bootdevice

on fs
     chown root system /mnt/vendor/persist
     chmod 0771 /mnt/vendor/persist
     restorecon_recursive /mnt/vendor/persist
     mkdir /mnt/vendor/persist/data 0700 system system
	chmod 0664 /sys/class/thermal/thermal_message/sconfig
    chown system system /sys/class/thermal/thermal_message/sconfig
    chmod 0666 /sys/class/thermal/thermal_message/temp_state
    chown system system /sys/class/thermal/thermal_message/temp_state
    chmod 0666 /sys/class/leds/lcd-backlight/brightness

on boot
    #Load WLAN driver
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules/ qca_cld3_wlan
    exec_background u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules/5.4-gki qca_cld3_wlan
    mkdir /data/vendor/thermal 0771 root system
    mkdir /data/vendor/thermal/config 0771 root system
    #Start mi_thermald
    start mi_thermald

on property:sys.boot_completed=1
    chown system system /sys/wifi/dump_in_progress
#pd-mapper
service vendor.pd_mapper /vendor/bin/pd-mapper
    class core
    user system
    group system
    

#service wpa_supplicant /vendor/bin/hw/wpa_supplicant \
#    -O/data/vendor/wifi/wpa/sockets -puse_p2p_group_interface=1 -dd \
#    -g@android:vendor_wpa_wlan0
#   we will start as root and wpa_supplicant will switch to user wifi
#   after setting up the capabilities required for WEXT
#   user wifi
#   group wifi inet keystore
#    interface android.hardware.wifi.supplicant@1.0::ISupplicant default
#    interface android.hardware.wifi.supplicant@1.1::ISupplicant default
#    interface android.hardware.wifi.supplicant@1.2::ISupplicant default
#    interface android.hardware.wifi.supplicant@1.3::ISupplicant default
#    interface vendor.qti.hardware.wifi.supplicant@2.0::ISupplicantVendor default
#    interface vendor.qti.hardware.wifi.supplicant@2.1::ISupplicantVendor default
#    interface vendor.qti.hardware.wifi.supplicant@2.2::ISupplicantVendor default
#    class main
#    socket vendor_wpa_wlan0 dgram 660 wifi wifi
#    disabled
#    oneshot
service mi_thermald /system/vendor/bin/mi_thermald
    class main
    user root
    group system
    seclabel u:r:mi_thermald:s0
	
#service hostapd_fst /vendor/bin/hw/hostapd -dd -g /data/vendor/wifi/hostapd/global
#    class main
#    capabilities NET_ADMIN NET_RAW
#    user wifi
#    group wifi
#    disabled
#    oneshot


#cnss_diag
#on property:vold.decrypt=trigger_restart_framework
#    start vendor.cnss_diag

service vendor.cnss_diag /vendor/bin/cnss_diag -q -f -s -t HELIUM
    class main
    user system
    group system wifi inet sdcard_rw media_rw diag
    capabilities NET_ADMIN
    disabled

#cnss-daemon
service cnss-daemon /system/vendor/bin/cnss-daemon -n -l
    class late_start
    user system
    group system inet net_admin wifi
    capabilities NET_ADMIN

on property:sys.shutdown.requested=*
    write /sys/kernel/shutdown_wlan/shutdown 1
    stop cnss-daemon

#GNSS
# msm specific files that need to be created on /data
on post-fs-data
    #Create directories for Location services
    mkdir /data/vendor/location 0770 gps gps
    mkdir /data/vendor/location/mq 0770 gps gps
    mkdir /data/vendor/location/xtwifi 0770 gps gps
    mkdir /dev/socket/location 0770 gps gps
    mkdir /dev/socket/location/mq 0770 gps gps
    mkdir /dev/socket/location/xtra 0770 gps gps
    mkdir /dev/socket/location/dgnss 0770 gps gps
    mkdir /data/vendor/wlan_logs 0770 system wifi

#service mlid /vendor/bin/mlid
#    class late_start
#    user gps
#    group gps
#    socket mlid stream 0666 gps gps

service loc_launcher /system/vendor/bin/loc_launcher
    class late_start
    user gps
    group gps

# 2022.01.08 wangxiang4 add start for tools of wifi RF test and start by dial *#*#2008#*#*
service openwifi_L /vendor/bin/sh /vendor/bin/wifitest.sh
    oneshot
    disabled

service closewifi_L /vendor/bin/sh /vendor/bin/wifitest_close.sh
    oneshot
    disabled

on property:odm.openwifi_L=1
    start openwifi_L

on property:odm.closewifi_L=1
    start closewifi_L

service wifiFtmdaemon /vendor/bin/ftmdaemon -dd -n
    oneshot
    disabled

on  property:vendor.sys.wifiFtmdaemon=1
    start wifiFtmdaemon

on  property:vendor.sys.wifiFtmdaemon=0
    stop wifiFtmdaemon

service myftmftm /vendor/bin/sh /vendor/bin/myftm.agent.sh
    oneshot
    disabled

on property:vendor.sys.myftm=1
    start myftmftm

service wdsdaemon /vendor/bin/wdsdaemon -su
    user root
    group root
    oneshot
    disabled

on property:odm.start_wdsdaemon=1
    start wdsdaemon

on property:odm.start_wdsdaemon=0
    stop wdsdaemon

service blueduttest /vendor/bin/sh /vendor/bin/bluedut.sh
    class late_start
    user root
    group root
    disabled
    oneshot

service btclose /vendor/bin/sh /vendor/bin/bt_close.sh
    user root
    group root
    oneshot
    disabled

on property:odm.closebt=1
    start btclose
# 2022.01.08 wangxiang4 add end

#2022.01.12 wangxiang4 add for wifi 9434 log
service vendor.tcpdump /system/vendor/bin/tcpdump -i any -W 2 -C 2 -s 134 -w /data/vendor/wlan_logs/tcpdump.pcap
    class main
    user root
    group root wifi inet sdcard_rw media_rw diag
    disabled
    oneshot

service sniffer /system/vendor/bin/tcpdump -i wlan0 -w /data/vendor/wlan_logs/sniffer.pcap
    class main
    user root
    group root
    disabled
    oneshot

on property:sys.user.0.ce_available=true
    start vendor.cnss_diag
    start vendor.tcpdump

service startpktlog /system/vendor/bin/iwpriv wlan0 pktlog 2
    class main
    user root
    group root
    disabled
    oneshot

service stoppktlog /system/vendor/bin/iwpriv wlan0 pktlog 0
    class main
    user root
    group root
    disabled
    oneshot
#2022.01.12 wangxiang4 add end
